echo "Converting PNG images to JPG images."
@echo off
for /r ".\" %%a in (*.png) do ffmpeg -v error -i "%%a" "%%~pa%%~na.jpg"
@echo on
if exist .\jpg\NUL (
    echo "Folder already exists, moving on."
)
if not exist .\jpg\NUL (
    echo "Folder doesn't exist, creating new one."
    @echo off
    mkdir .\jpg\
    @echo on
)
echo "Moving JPG images to jpg folder"
@echo off
for /r ".\" %%a in (*.jpg) do move "%%a" ".\jpg\"
@echo on