# Contimodded Screenshots
* This repo contains [JoubaMety](https://gitlab.com/joubamety/)'s Minecraft screenshots using [Continuum 2.0 Shaderpack](https://continuum.graphics/continuum-shaders/) with [JoubaMety](https://gitlab.com/joubamety/)'s private modifications and more.<br>
This modified shaderpack is dubbed as Contimodded.
* Click on picture to get redirected to PNG (original) version.

## #1: Lovely Night in Forest
<p align="center">
    <a href="2021-04-16_21.24.41.png" target="_blank" rel="noopener noreferrer">
        <img alt="Lovely Nights 1" src="jpg/2021-04-16_21.24.41.jpg" width="45%">
    </a>
    <a href="2021-04-16_21.25.40.png" target="_blank" rel="noopener noreferrer">
        <img alt="Lovely Nights 2" src="jpg/2021-04-16_21.25.40.jpg" width="45%">
    </a>
    <a href="2021-04-16_21.26.30.png" target="_blank" rel="noopener noreferrer">
        <img alt="Lovely Nights 3" src="jpg/2021-04-16_21.26.30.jpg" width="45%">
    </a>
    <a href="2021-04-16_21.27.59.png" target="_blank" rel="noopener noreferrer">
        <img alt="Lovely Nights 4" src="jpg/2021-04-16_21.27.59.jpg" width="45%">
    </a>
    <a href="2021-04-16_21.30.14.png" target="_blank" rel="noopener noreferrer">
        <img alt="Lovely Nights 5" src="jpg/2021-04-16_21.30.14.jpg" width="45%">
    </a>
    <a href="2021-04-16_21.32.03.png" target="_blank" rel="noopener noreferrer">
        <img alt="Lovely Nights 6" src="jpg/2021-04-16_21.32.03.jpg" width="45%">
    </a>
    <a href="2021-04-17_17.33.38.png" target="_blank" rel="noopener noreferrer">
        <img alt="Lovely Nights 6" src="jpg/2021-04-17_17.33.38.jpg" width="45%">
    </a>
</p>

* Date: 16.4. 2021 to 17.4. 2021
* Resolution: 3840x2160 (16:9)
* Shaderpack: [Contimodded](#contimodded-screenshots)
* Map & Resourcepack: Experiment Retribution by [Pasquale](https://twitter.com/Pasqual38900077)<br>
(Currently closed to Beta Testers on their Discord Server, might change in the future.)